import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';

import moment from "moment";



const TaskItem = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const currentDate = moment();
  const beginDate = moment(props.beginDate);
  const disabled = beginDate.isSameOrAfter(currentDate);


  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress} disabled={disabled}>
      <View style={[styles.subContainer, disabled && styles.disabled]}>
        {disabled && (<View style={styles.beginDateContainer}><Text style={styles.beginDate}>Begin {beginDate.fromNow()}</Text></View>)}

        <View style={styles.titleContainer}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View style={styles.pointsContainer}>
          <Text style={styles.points}>{props.points}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
};

TaskItem.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  points: PropTypes.number.isRequired,
  beginDate: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
  },
  subContainer: {
    borderRadius: theme.DEFAULT_RADIUS,
    backgroundColor: theme.GREEN_COLOR,
    flexDirection: 'row',
    height: 80,
  },
  disabled: {
    backgroundColor: theme.GRAY_COLOR_OPACITY_30
  },
  titleContainer: {
    flex: 3,
    justifyContent: 'center',
    marginLeft: 20,
  },
  pointsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    fontWeight: '600'
  },
  points: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    fontWeight: '600',
    justifyContent: 'center'
  },
  beginDateContainer: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  beginDate: {
    fontSize: theme.FONT_SIZE_SMALL,
    color: theme.FONT_COLOR,
    fontWeight: '600',
    padding: 5,
    opacity: 1,
  }
});

export default TaskItem;