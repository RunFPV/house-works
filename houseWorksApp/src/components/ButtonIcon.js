import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import theme from '../theme';


const ButtonIcon = props => {

    const styleFromProps = {};

    if (props.color) {
        styleFromProps.backgroundColor = props.color;
    } else {
        styleFromProps.backgroundColor = theme.PRIMARY_COLOR;
    }
    if (props.radius) {
        styleFromProps.borderRadius = props.radius;
    } else {
        styleFromProps.borderRadius = theme.DEFAULT_BUTTON_RADIUS;
    }

    return (
        <View style={props.disabled && styles.disabled}>
            <TouchableOpacity style={[styles.container, props.absolute && styles.absolute, styleFromProps]} onPress={props.onPress} disabled={props.disabled}>
                <FontAwesomeIcon size={theme.ICON_SIZE_LARGE} icon={props.icon} color={theme.WHITE_COLOR} />
            </TouchableOpacity>
        </View>

    );
};

ButtonIcon.propTypes = {
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
    color: PropTypes.string,
    absolute: PropTypes.bool,
    radius: PropTypes.number,
    disabled: PropTypes.bool,
}

const styles = StyleSheet.create({
    container: {
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
    },
    absolute: {
        position: 'absolute',
        bottom: 20,
        right: 20
    },
    disabled: {
        opacity: 0.5
    }
});

export default ButtonIcon;
