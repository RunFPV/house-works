import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import theme from '../theme';



const Separator = props => {
    return (
        <View style={styles.separator} />
    )
};

Separator.propTypes = {
}

const styles = StyleSheet.create({
    separator: {
        marginVertical: theme.LIST_SPACING,
    }
});

export default Separator;