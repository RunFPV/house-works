import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';


import { faTrophy } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import theme from '../theme';



const PositionItem = props => {

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress} disabled>
      <View style={styles.subContainer}>
        <View style={styles.posContainer}>
          <Text style={styles.pos}>{props.pos == 1 ? <FontAwesomeIcon color={theme.WHITE_COLOR} icon={faTrophy} size={theme.ICON_SIZE_LARGE} /> : "#" + props.pos}</Text>
        </View>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View style={styles.pointsContainer}>
          <Text style={styles.points}>{props.points}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
};

PositionItem.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  points: PropTypes.number.isRequired,
}

const styles = StyleSheet.create({
  container: {
  },
  subContainer: {
    borderRadius: theme.DEFAULT_RADIUS,
    backgroundColor: theme.PRIMARY_COLOR,
    flexDirection: 'row',
    height: 80,
  },
  posContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  pos: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    fontWeight: '600',
  },
  titleContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pointsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    fontWeight: '600'
  },
  points: {
    color: theme.WHITE_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    fontWeight: '600',
    justifyContent: 'center'
  }
});

export default PositionItem;