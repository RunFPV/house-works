import { DynamicValue } from 'react-native-dynamic';
import { DarkTheme } from '@react-navigation/native';

const color = {
    BLACK_COLOR: 'rgba(0, 0, 0, 1)',
    BLACK_SOFT_COLOR: 'rgba(22, 22, 22, 1)',
    WHITE_COLOR: 'rgba(255, 255, 255, 1)',
    WHITE_SOFT_COLOR: 'rgba(244, 244, 244, 1)',
    GOLD_COLOR: 'rgba(249, 202, 36, 1)',
    GREEN_COLOR: 'rgba(39, 174, 96, 1)',
    RED_COLOR: 'rgba(232, 65, 24, 1)',
    GRAY_COLOR: 'rgba(124, 124, 125, 1)',
    GRAY_COLOR_OPACITY_30: 'rgba(124, 124, 125, 0.3)'
};

const navDarkTheme = {
    ...DarkTheme,
    colors: {
        ...DarkTheme.colors,
        background: color.BLACK_COLOR,
        card: color.BLACK_COLOR
    }
}

console.warn(navDarkTheme);

export default theme = {
    BLACK_COLOR: color.BLACK_COLOR,
    BLACK_SOFT_COLOR: color.BLACK_SOFT_COLOR,
    WHITE_COLOR: color.WHITE_COLOR,
    WHITE_SOFT_COLOR: color.WHITE_SOFT_COLOR,
    GOLD_COLOR: color.GOLD_COLOR,
    GREEN_COLOR: color.GREEN_COLOR,
    RED_COLOR: color.RED_COLOR,
    GRAY_COLOR: color.GRAY_COLOR,
    GRAY_COLOR_OPACITY_30: color.GRAY_COLOR_OPACITY_30,
    BACKGROUND_COLOR: new DynamicValue(color.WHITE_COLOR, color.BLACK_COLOR),
    BACKGROUND_SOFT_COLOR: new DynamicValue(color.WHITE_SOFT_COLOR, color.BLACK_SOFT_COLOR),
    FONT_COLOR: new DynamicValue(color.BLACK_COLOR, color.WHITE_COLOR),
    ICON_COLOR: new DynamicValue(color.BLACK_COLOR, color.WHITE_COLOR),
    PRIMARY_COLOR: '#3498db',
    SECONDARY_COLOR: '#bdc3c7',
    // ICON
    ICON_SIZE_SMALL: 16,
    ICON_SIZE_MEDIUM: 20,
    ICON_SIZE_LARGE: 24,
    ICON_SIZE_XLARGE: 32,
    ICON_SIZE_3XLARGE: 64,
    // FONT
    FONT_SIZE_SMALL: 13,
    FONT_SIZE_MEDIUM: 17,
    FONT_SIZE_LARGE: 20,
    FONT_SIZE_XLARGE: 25,
    // FONT WEIGHT
    FONT_WEIGHT_MEDIUM: '500',
    // SPACING
    DEFAULT_SPACING: 12,
    HEADER_ICON_SPACING: 20,
    LIST_SPACING: 6,
    // RADIUS
    DEFAULT_BUTTON_RADIUS: 10,
    DEFAULT_RADIUS: 5,
    // DARK OR LIGHT
    BAR_STYLE: {
        DARK: 'dark-content',
        LIGHT: 'light-content'
    },
    DYNAMIC: {
        DARK: 'dark',
        LIGHT: 'light'
    },
    NAV_DARK_THEME: navDarkTheme,
};
