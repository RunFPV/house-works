import React from 'react';
import { SafeAreaView, View, Image, KeyboardAvoidingView } from 'react-native';
import { useForm } from 'react-hook-form';

import { faArrowRight, faArrowsAlt, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';
import { Logo } from '../image';

import ButtonText from '../components/ButtonText';
import TextInputForm from '../components/TextInputForm';


const LoginScreen = ({ navigation }) => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    console.log(data);
    props.toggleVisible(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{ flex: 1 }}>
        <View style={styles.subContainer}>
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={Logo} />
          </View>
          <TextInputForm placeholder="Email" name="email" errors={errors} control={control} keyboardType="email-address" />
          <View style={styles.separator} />
          <TextInputForm placeholder="Password" name="password" errors={errors} control={control} secureTextEntry />
          <View style={styles.separator} />
          <ButtonText icon={faSignInAlt} text="Login" onPress={handleSubmit(onSubmit)} />
          <View style={styles.separator} />
          <ButtonText iconRight icon={faArrowRight} text="Sign-up" color={null} onPress={() => navigation.navigate("Register")} />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    margin: theme.DEFAULT_SPACING,
    flex: 1
  },
  logoContainer: {
    flex: 1,
  },
  logo: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  separator: {
    marginVertical: theme.LIST_SPACING,
  },
});

export default LoginScreen;