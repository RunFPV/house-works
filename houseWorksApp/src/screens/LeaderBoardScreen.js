import React, { useState } from 'react';
import { View, Text, FlatList } from 'react-native';

import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';

import PositionItem from '../components/PositionItem';


const DATA = [
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        name: 'Ju la Saucisse',
        points: 100
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        name: 'Greg le Zgueg',
        points: 10
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        name: 'La chatounette',
        points: -100
    },
];

const LeaderBoardScreen = () => {
    const styles = useDynamicStyleSheet(dynamicStyles);
    const [fetching, setFetching] = useState(false);


    const onRefresh = () => {
        setFetching(true);
        console.log("call api");
        setFetching(false);
    };

    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <FlatList
                    onRefresh={onRefresh}
                    refreshing={fetching}
                    ListHeaderComponent={() => <Text style={styles.listTitle}>LeaderBoard</Text>}
                    contentContainerStyle={{ flexGrow: 1 }}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    data={DATA}
                    renderItem={({ item, index }) => (
                        <PositionItem
                            pos={index+1}
                            onPress={() => alert("done")}
                            beginDate={item.beginDate}
                            title={item.name}
                            disabled={item.disabled}
                            points={item.points} />
                    )}
                    keyExtractor={item => item.id}
                    ItemSeparatorComponent={() => (<View style={styles.separator} />)}
                />
            </View>
        </View>
    );
};

const dynamicStyles = new DynamicStyleSheet({
    container: {
        backgroundColor: theme.BACKGROUND_COLOR,
        flex: 1,
    },
    subContainer: {
        marginHorizontal: theme.DEFAULT_SPACING,
        flex: 1,
    },
    listTitle: {
        color: theme.FONT_COLOR,
        fontSize: theme.FONT_SIZE_XLARGE,
        paddingVertical: 10,
    },
    separator: {
        marginVertical: theme.LIST_SPACING,
    },
});

export default LeaderBoardScreen;