import React, { useState } from 'react';
import { View, Text, FlatList } from 'react-native';

import EditModal from '../components/EditModal';
import TouchableIcon from '../components/TouchableIcon';
import TaskItem from '../components/TaskItem';

import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';


const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Passer l\'aspiratueur',
    points: 10,
    beginDate: '2021-06-09T23:47:00'
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Petite gâterie à mon coeur',
    points: 100,
    beginDate: '2021-06-10T00:00:00'
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Gros nettoyage de la douche, avec usage de javel',
    points: 30,
    beginDate: '2021-08-20'
  },
];

const HomeScreen = ({ navigation }) => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [showModal, setShowModal] = useState(false);
  const [fetching, setFetching] = useState(false);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableIcon
          color={theme.GRAY_COLOR}
          icon={faPlus}
          onPress={() => setShowModal(true)}
          style={{ padding: theme.HEADER_ICON_SPACING }} />
      ),
    });
  }, [navigation]);

  const onRefresh = () => {
    setFetching(true);
    console.log("call api");
    setFetching(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <FlatList
          onRefresh={onRefresh}
          refreshing={fetching}
          ListHeaderComponent={() => <Text style={styles.listTitle}>Tasks</Text>}
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          data={DATA}
          renderItem={({ item }) => (
            <TaskItem
              onPress={() => alert("done")}
              beginDate={item.beginDate}
              title={item.title}
              disabled={item.disabled}
              points={item.points} />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={() => (<View style={styles.separator} />)}
        />
      </View>

      <EditModal title="Add" visible={showModal} toggleVisible={() => setShowModal(false)} />
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    marginHorizontal: theme.DEFAULT_SPACING,
    flex: 1,
  },
  listTitle: {
    color: theme.FONT_COLOR,
    fontSize: theme.FONT_SIZE_XLARGE,
    paddingVertical: 10,
  },
  separator: {
    marginVertical: theme.LIST_SPACING,
  },
});

export default HomeScreen;