import React from 'react';
import { SafeAreaView, View, Text, KeyboardAvoidingView } from 'react-native';
import { useForm } from 'react-hook-form';

import { faArrowRight, faHouseUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';

import ButtonText from '../components/ButtonText';
import TextInputForm from '../components/TextInputForm';
import Separator from '../components/Separator';


const CreateHomeScreen = ({ navigation }) => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    console.log(data);
    props.toggleVisible(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{ flex: 1 }}>
        <View style={styles.subContainer}>
          <View style={styles.logoContainer}>
            <FontAwesomeIcon style={styles.logo} icon={faHouseUser} color={theme.WHITE_COLOR} size={theme.ICON_SIZE_3XLARGE} />
            <Text style={styles.title}>Create a new Home</Text>
          </View>
          <Separator />
          <TextInputForm placeholder="Home name" name="homeName" errors={errors} control={control} keyboardType="email-address" />
          <Separator />
          <ButtonText text="Create Home" onPress={handleSubmit(onSubmit)} />
          <Separator />
          <ButtonText iconRight icon={faArrowRight} text="Join existing Home" color={null} onPress={() => navigation.navigate("JoinHome")} />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    margin: theme.DEFAULT_SPACING,
    flex: 1
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
  },
  title: {
    fontSize: theme.FONT_SIZE_XLARGE,
    color: theme.FONT_COLOR,
  }
});

export default CreateHomeScreen;