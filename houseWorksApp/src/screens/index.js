import HomeScreen from './HomeScreen';
import SettingsScreen from './SettingsScreen';
import LeaderBoardScreen from './LeaderBoardScreen';
import CreateHomeScreen from './CreateHomeScreen';
import JoinHomeScreen from './JoinHomeScreen';
import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';

export {
    HomeScreen,
    SettingsScreen,
    LeaderBoardScreen,
    CreateHomeScreen,
    JoinHomeScreen,
    LoginScreen,
    RegisterScreen
};