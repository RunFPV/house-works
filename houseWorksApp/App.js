/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// React Navigation
import 'react-native-gesture-handler';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import React from 'react';
import type { Node } from 'react';
import { useColorScheme } from 'react-native';
import AppNavigator from './src/navigation/AppNavigator';
import { DarkModeProvider, useDarkMode } from 'react-native-dynamic';
import { StatusBar } from 'react-native';


const App: () => Node = () => {
  const deviceUseDarkMode = useDarkMode();

  let dynamic = theme.DYNAMIC.LIGHT;
  let barStyle = theme.BAR_STYLE.DARK;
  let navigationTheme = DefaultTheme;

  if (deviceUseDarkMode) {
    dynamic = theme.DYNAMIC.DARK;
    barStyle = theme.BAR_STYLE.LIGHT;
    navigationTheme = theme.NAV_DARK_THEME;
  }

  return (
    <DarkModeProvider mode={dynamic}>
      <NavigationContainer theme={navigationTheme}>
      <StatusBar barStyle={barStyle} />
        <AppNavigator />
      </NavigationContainer>
    </DarkModeProvider>
  );
};

export default App;
